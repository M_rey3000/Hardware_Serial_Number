package hw_sn;

import java.util.Scanner;

/**
 *
 * @author M_rey3000
 */
public class HW_SN {

    public static void main(String[] args) throws Throwable {
        // wmic command for diskdrive id: wmic DISKDRIVE GET SerialNumber
        // wmic command for cpu id : wmic cpu get ProcessorId
        // wmic command for bios :wmic bios get serialnumber
        // wmic cpu get name
        // wmic csproduct get uuid
        // wmic cpu get caption
        Process process = Runtime.getRuntime().exec(new String[] { "wmic", "csproduct", "get", "uuid" });
        

        process.getOutputStream().close();
        Scanner sc = new Scanner(process.getInputStream());
        String property = sc.next();
        String serial = sc.next();
        System.out.println(property + ": " + serial);
    }
}